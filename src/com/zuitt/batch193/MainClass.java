package com.zuitt.batch193;

import java.util.Scanner;

public class MainClass {

    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");

        int num = input.nextInt();

        int x = 1;

        for(int i = 1; i <= num; i++){
            x *= i;
        }

        System.out.println("The factorial of " + num + " is " + x);

    }

}
